import json
import os
import threading
import subprocess
import shutil
from boto3.s3.transfer import S3Transfer


def upload(transfer, file_name, bucket, object_key):
    if '.html' in file_name:
        transfer.upload_file(
            file_name, bucket, object_key, extra_args={
                'ServerSideEncryption': "AES256",
                'ContentType': 'text/html'
            })
    elif '.json' in file_name:
        transfer.upload_file(
            file_name, bucket, object_key, extra_args={
                'ServerSideEncryption': "AES256",
                'ContentType': 'application/json'
            })
    elif '.js' in file_name:
        transfer.upload_file(
            file_name, bucket, object_key, extra_args={
                'ServerSideEncryption': "AES256",
                'ContentType': 'application/javascript'
            })
    elif '.css' in file_name:
        transfer.upload_file(
            file_name, bucket, object_key, extra_args={
                'ServerSideEncryption': "AES256",
                'ContentType': 'text/css'
            })
    else:
        transfer.upload_file(
            file_name, bucket, object_key, extra_args={
                'ServerSideEncryption': "AES256"
            })


def delete(s3_resource, bucket_name, file_name):
    s3_resource.Object(bucket_name, file_name).delete()


class Deploy(object):
    def __init__(self, boto_session, aws_credentials, app_deploy_config, bucket_name, env, client_path, folder,
                 ui_bucket):
        self.boto_session = boto_session
        self.aws_credentials = aws_credentials
        self.app_deploy_config = app_deploy_config
        self.bucket_name = bucket_name
        self.env = env
        self.client_path = client_path
        self.folder = folder
        self.ui_bucket = ui_bucket

        self.region_name = aws_credentials.region_name
        self.s3_client = boto_session.client(service_name='s3', region_name=self.region_name)
        self.s3_resource = boto_session.resource(service_name='s3', region_name=self.region_name)
        self.transfer = S3Transfer(self.s3_client)

    def delete_client_ui(self, bucket_name, bucket_prefix):
        bucket = self.s3_resource.Bucket(bucket_name)
        delete_keys = []
        for obj in bucket.objects.filter(Prefix=bucket_prefix):
            delete_keys.append(obj.key)

        for delete_key in delete_keys:
            thread = threading.Thread(
                target=delete, args=(self.s3_resource, bucket_name, delete_key))
            thread.start()

            if threading.activeCount() == 100:
                thread.join()

    def deploy_client_config(self):
        env_config = json.load(open(self.client_path))
        clients = env_config["clients"]
        for client in clients:
            self.copy_client_config(client)

    def deploy_ui(self):
        env_config = json.load(open(self.client_path))
        clients = env_config["clients"]
        root_dir = os.path.join(self.folder, self.app_deploy_config['app_ui_base'], 'data-catalog')
        # environment_dir = os.path.join(root_dir, 'src/environments/environment.prod.ts')
        print("pushd \"" + root_dir + "\" && npm install && popd")
        os.system("pushd \"" + root_dir + "\" && npm install && popd")
        print("pushd \"" + root_dir + "\" && npm run build-prod && popd")
        os.system("pushd \"" + root_dir + "\" && npm run build-prod  && popd")
        dis_dir = os.path.join(root_dir, 'dist/data-catalog')
        file_names = []
        destination = dis_dir + "/assets/config.json"
        for r, d, f in os.walk(dis_dir):
            for file in f:
                rel_dir = os.path.relpath(r, dis_dir)
                rel_file = os.path.join(rel_dir, file)
                if '.\\' in rel_file:
                    rel_file = rel_file.split('\\')[1]
                file_names.append(rel_file.replace('\\', '/'))
        for client in clients:
            # shutil.copyfile('Integration/' + client['StageClientName'] + '/environment.ts', environment_dir)
            # os.rename(os.path.join(root_dir, 'dist'), os.path.join(root_dir,'dist_' + client['StageClientName']))
            # dis_dir = os.path.join(root_dir, 'dist_' + client['StageClientName'], 'data-catalog')
            source = 'Integration/' + client['StageClientName'] + '/config.json'
            # Copy config.json file
            shutil.copyfile(source, destination)
            bucket_name = self.ui_bucket
            bucket_prefix = str(self.env + '/' + client['StageClientName'])
            # Delete client bucket
            self.delete_client_ui(bucket_name, bucket_prefix)

            thread = None
            # Copy client UI files
            for file_name in file_names:
                thread = threading.Thread(target=upload, args=(
                    self.transfer, dis_dir + '/' + file_name, self.ui_bucket, self.env + '/' + client['StageClientName'] + '/web/' + file_name))
                thread.start()
                if threading.active_count() == 100:
                    thread.join()
            if threading.active_count() > 1:
                thread.join()

    def copy_client_config(self, client):
        file_name = os.path.join('Integration/' + client['StageClientName']) + '/' + client['stageVariables']['BUCKET_KEY']
        bucket = client['stageVariables']['BUCKET_NAME']
        object_key = client["StageClientName"] + "/" + client['stageVariables']['BUCKET_KEY']

        # upload backend config
        upload(self.transfer, file_name, bucket, object_key)